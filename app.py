from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/status/<int:code>")
def status_code(code):
    if code == 200:
        return jsonify({"status": "OK"}), 200
    elif code == 400:
        return jsonify({"error": "Bad Request"}), 400
    elif code == 500:
        return jsonify({"error": "Internal Server Error"}), 500
    else:
        return jsonify({"error": "Invalid status code"}), 400

if __name__ == "__main__":
    app.run()
